using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    [SerializeField] float sceneLoadDelay = 2f;
    ScoreKeeper scKeeper;

    void Start() {
        
        //Debug.Log(scKeeper);
    }

    public void LoadGame(){
        scKeeper = FindObjectOfType<ScoreKeeper>();
        Debug.Log(scKeeper);
        scKeeper.ResetScore();
        SceneManager.LoadScene(1);
    }
    public void LoadMenu(){
        SceneManager.LoadScene(0);
    }
    public void LoadGameOver(){

        StartCoroutine(WaitAndLoad(2,sceneLoadDelay));
    }
    public void QuitGame(){
        Application.Quit();
        Debug.Log("Quitting game...");
    }

    IEnumerator WaitAndLoad(int scene, float delay){
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene(scene);
    }
}
